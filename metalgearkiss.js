var twitter = require('twitter'),
    fs = require('fs'),
    http = require('http');

fs.readFile("settings.conf", {encoding: 'utf8'}, function(err, data) {
    if (err) {
        console.error(err);
    }
    conf = JSON.parse(data);
    
    var client = new twitter(conf.twitter_conf);
    fs.readFile("revscriptfilteredtruncated.json", {encoding: 'utf8'}, function(err, data) {
        fs.readFile("exclusion.json", {encoding: 'utf8'}, function(err, exclusion) {
            data = JSON.parse(data);
            setInterval(tweet, 60 * 60 * 8 * 1000, data, exclusion);
        });
    });
});


function tweet(data, exclusion) {
    phrase = data[Math.round(Math.random() * data.length - 1)];
        var name = phrase.split(":")[0];
        var splitPhrase = phrase.split(":")[1].split(" ");
        splitPhrase.splice(0, 1);
        //loop through words in line
        var sanity = 0;
        while(true) {
            var index = Math.floor((splitPhrase.length) * Math.random());
            if (!exclusion.includes(splitPhrase[index])) {
                console.log("replaced word: " + splitPhrase[index]);
                if (splitPhrase[index]) {
                    if (splitPhrase[index].charAt(0).toUpperCase() == splitPhrase[index].charAt(0)) {
                        splitPhrase[index] = "Kiss";
                    } else {
                        splitPhrase[index] = "kiss";
                    }
                } else {
                    console.log(splitPhrase);
                    console.log(index);
                }
                break;
            } else {
                console.log("rejected: " + splitPhrase[index]);
                sanity += 1;
                if (sanity > 50) {
                    break;
                }
            }
        }
        var output = name + ": " + splitPhrase.join(" ");
        client.post("statuses/update", {status: output}, function(data, textStatus, jqXHR) {
            console.log(data);
        });
}